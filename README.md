## Overview
By default, maven packages are using .JAR extension. JAR stand for Java Archive, it is built based on the ZIP format and is used to aggregate many file Java class files and corresponding metadata and resources into a single file.   

In maven project, we declare a new dependencies by adding it to pom.xml file. By doing that process, actually we are consuming a remote JAR and its content, allowing us to use external libraries.  And by the same analogy, we can also consume our internal and private remote JAR repository for our next maven project as a dependencies.

Regarding this scenario, Gitlab offers us a Package Registry to store several packages types such as Maven, Go, npm, and so on. This integration enabled us to stored our JAR files inside Package Registry to be consumed next time which is inline with our initial needs. 

In this step-by-step guide, I will demonstrate how to deploy Maven's packages to Gitlab’s Package Registry using via CI/CD. And how to consume your Maven's packages stored in Gitlab's Package Registry.

With this guide you will be able to publish your Maven's artifacts and packages in your Gitlab Project's Package Registry, you can also install the packages as dependencies from another Maven project in the same Gitlab Group.

## Source Project Installation Step (Publisher)
In this guides I will assume that we still haven't create the maven project, so we will start by creating the maven project. If you already have a maven project you want to deploy to Gitlab Package Registry then you can skip the first step.
### Create Maven Project
You can follow this step create new maven project.
1. Enter or create a directory to store our maven project. For example we will use /home/user/maven
```
cd /home/user/maven
```
2. Create new maven project using this command
```
mvn archetype:generate -DgroupId=xyz.geekinthecloud.portfolio -DartifactId=source-project -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false
```
> You can change the following details
> 1. DgroupId : a unique string to identified your packages, for this guide I used my reverse domain name as example.
> 2. DartifactId : the name of the packages `(JAR files)` build by maven, will append in the end of groupId.
> 3. DarchetypeArtifactId : The archetype used to create the initial structure of the project.
> 4. DinteractiveMode : Create the project using batch mode (optional).

### Authenticate Maven Project to the Gitlab's Package Registry
To authenticate Maven to the Package Registry we can choose to use A personal access token, A deploy token, and a `CI_JOB_TOKEN`. 
For this guide we will be using `CI_JOB_TOKEN`. We need to add the following section to our `settings.xml` file or we can make different `settings.xml` for maven to used. I will use new `settings.xml` called `ci_settings.xml`.
```
<settings>
  <servers>
    <server>
      <id>gitlab-maven</id>
      <configuration>
        <httpHeaders>
          <property>
            <name>Job-Token</name>
            <value>${CI_JOB_TOKEN}</value>
          </property>
        </httpHeaders>
      </configuration>
    </server>
  </servers>
</settings>
```
> Note : You must use Job-Token as the property name.

### Setup Gitlab Endpoint For Maven Packages
We have 3 options to be used in this setup process: **Project-level, Group-Level, Instance-level**. And because we will consume the packages from different projects in the same group so we will choose **Group-level**.

This code snippet shows the relevant repository section of your `pom.xml` file. As you can see you still need a project-specific URL specified with `PROJECT_ID` for publishing a package in the distributionManagement section, however for the repository section you should use `GROUP_ID`. 
```
<repositories>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.example.com/api/v4/groups/GROUP_ID/-/packages/maven</url>
  </repository>
</repositories>
<distributionManagement>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.example.com/api/v4/projects/PROJECT_ID/packages/maven</url>
  </repository>
  <snapshotRepository>
    <id>gitlab-maven</id>
    <url>https://gitlab.example.com/api/v4/projects/PROJECT_ID/packages/maven</url>
  </snapshotRepository>
</distributionManagement>
```
**Note:** Change the example.com to your gitlab domain name. If you are using gitlab.com then change the domain from gitlab.example.com to gitlab.com Please make sure to change the GROUP_ID and PROJECT_ID with your GROUP_ID and PROJECT_ID from your gitlab like this image:
![GROUP_ID.png](https://cdn.hashnode.com/res/hashnode/image/upload/v1661067408500/PAbiNEPOp.png align="left")
![POJECT_ID.png](https://cdn.hashnode.com/res/hashnode/image/upload/v1661067391336/lPKtxJtQO.png align="left")

### Create Gitlab CI/CD in Source App
After making sure the `pom.xml` file already contains all the dependencies we need, we just have to start the deployment process. We will use Gitlab CI for this, so create and edit `.gitlab-ci.yml` file with following code.
```
maven-build:
  image: maven:latest
  script: 
    - mvn deploy -s ci_settings.xml -DskipTests
``` 
### Push Your Change to Gitlab
Commit and push your changes to Gitlab using git. And wait for the pipeline to automatically deploy the maven packages to Gitlab Packages Registry. If you follow the guide above, your pipeline should work perfectly as shown bellow

![pipeline-status.png](https://cdn.hashnode.com/res/hashnode/image/upload/v1661068923859/tFpIs3dsJ.png align="left")
And you should also be able to find your newly deployed maven packages in the Gitlab Packages Registry menu. 

![package-registry.png](https://cdn.hashnode.com/res/hashnode/image/upload/v1661068932370/qne75VRst.png align="left")

## App Project Installation Step [Consumer]
After setting up our private Maven's packages registry inside **Group Level** Gitlab Package Registry. We can now use this registry in our next project. 
### Create New Maven Project
I have to mention one more time that I assumes you have not create a Maven project for both the publisher and consumer. So if you already have your own maven project you can skip this step.

You can install the maven project using the same command with the above step.
```
mvn archetype:generate -DgroupId=xyz.geekinthecloud.portfolio -DartifactId=app-project -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false
```

### Configure POM Dependencies
To ensure your maven project to download the package we deploy before, we have to add that package as artifact dependency in the `pom.xml` file of consumer project.  
```
<dependency>
  <groupId>xyz.geekinthecloud.portfolio</groupId>
  <artifactId>source-project</artifactId>
  <version>0.1.0-SNAPSHOT</version>
</dependency>
```
We also need to add this code snippet inside our `pom.xml`, so that our project can find all the packages we have deploy to Gitlab Package Registry
```
<repositories>
   <repository>
     <id>gitlab-maven</id>
     <url>https://gitlab.com/api/v4/groups/56888756/-/packages/maven</url>
   </repository>
</repositories>
```
### Testing
If you follow this step correctly you should be able to install your maven consumer project. I used Gitlab Runner to install the maven but you can also install it in your own environment directly as long as you have access to the gitlab registry.

You can see in this pipeline that our consumer project are requesting then downloading the publisher project's package
![downloading-pipeline.png](https://cdn.hashnode.com/res/hashnode/image/upload/v1661071748353/kGLAM_s8R.png align="left")

---
